<?php

namespace App\Http\Controllers;

use App\Http\Requests\NotesValidation;
use App\Http\Resources\NoteResource;
use App\Repositories\Notes\NoteRepository;
use Illuminate\Http\Request;
use Validator;

class NoteController extends Controller
{
    private $note;

    public function __construct( NoteRepository $note )
    {
        $this->note = $note;
    }

    public function store(NotesValidation $request){
        $note = $this->note->create($request->all());
        return (new NoteResource($note))->additional(['status'=>true]);
    }

    public function getAllNotes(){
        $notes = $this->note->getAll();
        return NoteResource::collection($notes)->additional(['status'=>true]);
    }

    public function show($id){
        $note = $this->note->getById($id);
        return (new NoteResource($note))->additional(['status'=>true]);
    }

    public function edit($id){
        $note = $this->note->getById($id);
        return (new NoteResource($note))->additional(['status'=>true]);
    }

    public function update(NotesValidation $request, $id){
        $note = $this->note->update($id , $request->all());
        return (new NoteResource($note))->additional(['status'=>true]);
    }

    public function destroy($id)
    {
        $this->note->delete($id);
        return response()->json(['status'=>true ]);
    }

}


