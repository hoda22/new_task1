<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $hidden=['updated_at'];
    protected $guarded = ['id'];
}
