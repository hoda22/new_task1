<?php
/**
 * Created by PhpStorm.
 * User: 7oda
 * Date: 11/15/2019
 * Time: 10:19 PM
 */

namespace App\Repositories\Notes;


interface NoteRepository
{
    public function getAll();

    public function getById($id);

    public function create(array $attributes);

    public function update($id , array $attributes);

    public function delete($id);

}