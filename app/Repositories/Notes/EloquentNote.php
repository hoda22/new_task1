<?php
/**
 * Created by PhpStorm.
 * User: 7oda
 * Date: 11/15/2019
 * Time: 10:25 PM
 */

namespace App\Repositories\Notes;


use App\Note;

class EloquentNote implements NoteRepository
{
    private $model;

    /**
     * @param Note $model
     */

    public function __construct(Note $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->paginate(10);
        // TODO: Implement getAll() method.
    }

    public function getById($id)
    {
        return $this->model->find($id);
        // TODO: Implement getById() method.
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
        // TODO: Implement create() method.
    }

    public function delete($id)
    {
        $this->getById($id)->delete();
        return true;
        // TODO: Implement delete() method.
    }

    function update($id, array $attributes)
    {
        $note = $this->getById($id);
        $note->update($attributes);
        return $note;
        // TODO: Implement update() method.
    }
}