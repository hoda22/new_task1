<?php

namespace App\Providers;
use App\Note;
use App\Observers\NotesObserver;
use App\Repositories\Notes\EloquentNote;
use App\Repositories\Notes\NoteRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NoteRepository::class , EloquentNote::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Note::observe(NotesObserver::class);
    }
}
