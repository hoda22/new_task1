<?php

namespace App\Observers;

use App\Note;
use App\Notifications\SystemNoti;
use App\User;

class NotesObserver
{
    /**
     * Handle the note "created" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function created(Note $note)
    {
        $users = User::all();
        foreach ( $users as $user ){
            $user->notify( new SystemNoti( $note->id ,$note->title )  );
        }
    }

    /**
     * Handle the note "updated" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function updated(Note $note)
    {
        //
    }

    /**
     * Handle the note "deleted" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function deleted(Note $note)
    {
        //
    }

    /**
     * Handle the note "restored" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function restored(Note $note)
    {
        //
    }

    /**
     * Handle the note "force deleted" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function forceDeleted(Note $note)
    {
        //
    }
}
