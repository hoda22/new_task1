<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'], function(){
//    Route::middleware('auth:api')->get('/user', function (Request $request) {
//        return $request->user();
//    });

    Route::get('notes','NoteController@getAllNotes');
    Route::post('note','NoteController@store');
    Route::get('note/{id}','NoteController@show');
    Route::post('note/{id}', 'NoteController@update');
    Route::delete('note/{id}','NoteController@destroy');



    Route::group([ 'middleware' => 'api', 'prefix' => 'auth'], function ($router) {

        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');

    });

});
